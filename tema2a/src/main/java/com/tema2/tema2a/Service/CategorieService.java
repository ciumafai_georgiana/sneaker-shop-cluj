package com.tema2.tema2a.Service;

import com.tema2.tema2a.Entity.Categorie;
import com.tema2.tema2a.Repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorieService implements IService<Categorie,Integer> {
    @Autowired
    private CategorieRepository categorieRepository;
    @Override
    public boolean add(Categorie element) {
        try {
        categorieRepository.save(element);
        return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public List<Categorie> getAll() {
        try {
            return categorieRepository.findAll();
        }catch (Exception e){
            return null;
        }

    }

    @Override
    public boolean getById(Integer id) {
        return categorieRepository.existsById(id);
    }

    public void deleteById(int id){
        categorieRepository.deleteCategorieById(id);
    }
}
