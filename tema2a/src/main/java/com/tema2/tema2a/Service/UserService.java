package com.tema2.tema2a.Service;


import com.tema2.tema2a.Entity.User;
import com.tema2.tema2a.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IService<User, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean add(User element) {
        try {
            userRepository.save(element);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public List<User> getAll() {
        try {
            return userRepository.findAll();
        }
        catch (Exception ex){
            return null;
        }
    }

    @Override
    public boolean getById(String email) {
        return userRepository.existsById(email);
    }


    public boolean getUserByEmailAndPassword(String email, String password){
        if(userRepository.getUserByEmailAndPassword(email, password) > 0){
            return true;
        }
        return false;
    }
    public Optional<User> getUser(String email) {
        return userRepository.findById(email);
    }



    public boolean deleteUser(String email){
        try {
            userRepository.deleteById(email);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    public boolean update(User user){
        try {
            userRepository.save(user);
            return true;
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
    public int getUserByIdUser(String email){
        try{
            return userRepository.getUserByIdUser(email);
        }catch(Exception e){
            return 0;
        }
    }
    /*
    public List<User> getJobProviders(int jobId)
    {
        return userRepository.getJobProviders(jobId);
    }*/

}
