package com.tema2.tema2a.Service;

import com.tema2.tema2a.Entity.ComandaProdus;
import com.tema2.tema2a.Repository.ComandaProdusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ComandaProdusService implements IService<ComandaProdus,Integer> {

    @Autowired
    ComandaProdusRepository comandaProdusRepository;

    @Override
    public boolean add(ComandaProdus element) {
        try {
            comandaProdusRepository.save(element);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public List<ComandaProdus> getAll() {
        return comandaProdusRepository.findAll();
    }

    @Override
    public boolean getById(Integer id) {
        return comandaProdusRepository.existsById(id);

    }
    public void deleteComandaProdusByidCP(int idCP)
    {
        comandaProdusRepository.deleteComandaProdusByidCP(idCP);
    }
    public void deleteComandaProdusByidP(int idP)
    {
        comandaProdusRepository.deleteComandaProdusByIdP(idP);
    }
    public void deleteComandaProdusByidPidC(int idP,int idC)
    {
        comandaProdusRepository.deleteComandaProdusByIdPAndIdC(idP,idC);
    }

    public void findComandaProdusByidC(int idC) {
        comandaProdusRepository.findComandaProdusByIdC(idC);

    }
    public void findComandaProdusByidCidP(int idP,int idC) {
        comandaProdusRepository.findComandaProdusByIdPAndIdC(idP,idC);

    }
}
