import * as React from 'react';
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { LoginService } from '../Services/LoginService.js';
import '../css/Login.css'

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submit();
    }
  }

  submit() {
    let loginModel = {
      email: this.state.email,
      password: this.state.password
    };
    LoginService.login(loginModel, this.state.type)
      .then(
        (response) => {
          LoginService.setData(this.state.email);

          this.props.history.push("/dashboard");
          sessionStorage.setItem("email",this.state.email);
        },
        (error) => {
          console.log("ERROR AT LOGIN" + error);
        });
  }

  render() {
    return (
      <React.Fragment>
        {
          <React.Fragment >
            <div className="container">
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4">
                  <InputText placeholder="Email utilizator" type="text" size={30} onChange={(event) => this.setState({ email: event.target.value })} />
                </div>
                <br></br>
                <div className="p-col-12 p-md-4">
                  <InputText placeholder="Parola" type="password" onKeyPress={this.handleKeyPress} size={30} onChange={(event) => this.setState({ password: event.target.value })} />

                </div>
                <div >
                  <Button className='btnn' label="Login" style={{ 'marginTop': `20px`, 'width': '48%', 'marginRight': '5%' }} onClick={(e) => {
                    this.submit(e);
                  }} />
                  { <Button className='btnn' label="Inregistrare" style={{ 'marginTop': `5px`, 'width': '47%' }} onClick={(e) => {
                    this.props.history.push("/register");
                  }} /> }
                </div>
              </div>
            </div>

          </React.Fragment>
        }
      </React.Fragment>
    )
  }
}
