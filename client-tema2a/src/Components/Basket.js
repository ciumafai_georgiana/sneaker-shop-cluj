import * as React from 'react';
import { Button } from 'primereact/components/button/Button';
//import { RestaurantService } from '../Services/RestaurantService';
import axios from 'axios';
import {DataView, DataViewLayoutOptions} from 'primereact/dataview';
export class Basket extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        basketId: [],
        basketNume: [],
        basketPrice: [],
        products:[],
        email:null

      }
    }
    loadBasket(){
        const names1=sessionStorage.getItem("basketNume");
        const ide=sessionStorage.getItem("email");
        this.setState({email:ide});
        this.setState({names:names1});
        var retrievedData = localStorage.getItem("bp");
        var pr = JSON.parse(retrievedData);
        this.setState({products:pr});

    
        console.log(this.state.products);
       
    }
    componentDidMount() {
       this.loadBasket();
    }
    itemTemplate(product, layout) {
        if (layout === 'list') {
            console.log(
                [product.price].reduce((a, b) => a + b, 0)
              )
            return (
                <div className="p-grid">
                    <div>{product.nume}</div>
                    <div>{product.price}</div>
                    
                </div>
                
            );
            
        }
    }
    render() {
      
        var sum = 0;
        this.state.products.forEach(function(obj){
        sum += obj.price;
        });
        console.log(this.state.email);
        return (
            <div>
                

             <DataView className='p-user-list' value={this.state.products}  layout={this.state.layout} itemTemplate={this.itemTemplate}></DataView> 
             <div>Total {sum}</div>
             <Button className="p-button-success"label={"Place order"} style={{ 'color':'white','marginTop': `5px`, 'width': '47%' }}
             
             onClick = {()=>{
                this.props.history.push("/order");
                  
            }}
             
             ></Button>
            </div>
        )                     
    }
}
