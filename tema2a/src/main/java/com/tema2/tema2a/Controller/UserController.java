package com.tema2.tema2a.Controller;


import com.tema2.tema2a.Entity.User;
import com.tema2.tema2a.Exception.ApplicationException;
import com.tema2.tema2a.Model.LoginModel;
import com.tema2.tema2a.Service.UserService;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/Users")
public class UserController {


    @Autowired
    UserService userService;
    private static boolean loginValidate(String username, String password){
        if(username.isEmpty() || username.equals(""))
            return false;
        if(password.isEmpty() || password.equals(""))
            return false;
        return true;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginModel loginModel) throws ApplicationException {
        if(!loginValidate(loginModel.getEmail(), loginModel.getPassword())){
            throw new ApplicationException("Empty fields", Response.SC_BAD_REQUEST);
        }

        try {
            if(userService.getUserByEmailAndPassword(loginModel.getEmail(), loginModel.getPassword())){
                return new ResponseEntity<>("Successful login", HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Failed login", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            throw new ApplicationException(e.getMessage(), Response.SC_UNAUTHORIZED);
        }
    }
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@RequestParam(value = "email", required = true) String email) throws ApplicationException {
        if(userService.getUser(email) ==  null){
            throw new ApplicationException("User with this email not exists", Response.SC_NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(userService.getUser(email), HttpStatus.OK);
        }
    }
    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public ResponseEntity<?> updateUser(@RequestBody User user) throws ApplicationException {
        if(userService.getById(user.getEmail()) == false){
            throw new ApplicationException("Account with this username doesn't exist", Response.SC_NOT_FOUND);
        }
        else {
            if (userService.update(user)){
                return new ResponseEntity<>("Successful update account user", HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Faild update account user", HttpStatus.BAD_REQUEST);
            }
        }
    }
    @PostMapping(value = "/register")
    public void addUser(@RequestBody User u ){
        userService.add(u);
    }
    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> all2 = userService.getAll();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(all2);
    }
}
