package com.tema2.tema2a.Repository;


import com.tema2.tema2a.Entity.ComandaProdus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ComandaProdusRepository extends JpaRepository<ComandaProdus,Integer> {

    @Query(value = "delete from comandaprodus where idcp=?1 ",nativeQuery = true)
    @Modifying
    @Transactional
    void deleteComandaProdusByidCP(int idCP);

    @Query(value = "delete from comandaprodus where idp=?1 ",nativeQuery = true)
    @Modifying
    @Transactional
    void deleteComandaProdusByIdP(int idP);

    @Query(value = "delete from comandaprodus where idp=?1 and idc=?2",nativeQuery = true)
    @Modifying
    @Transactional
    void deleteComandaProdusByIdPAndIdC(int idP,int idC);

    @Query(value = "SELECT *from comandaprodus where idp=?1 and idc=?2",nativeQuery = true)
    public List<ComandaProdus> findComandaProdusByIdPAndIdC(int idP,int idC);

    @Query(value = "SELECT *from comandaprodus where idc=?1 ",nativeQuery = true)
    public List<ComandaProdus> findComandaProdusByIdC(int idC);
}

