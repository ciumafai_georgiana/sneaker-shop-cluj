package com.tema2.tema2a.Controller;


import com.tema2.tema2a.Entity.Categorie;
import com.tema2.tema2a.Entity.Produs;
import com.tema2.tema2a.Service.ComandaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/Comanda")
public class ComandaController {

    @Autowired
    ComandaService comandaService;


    @RequestMapping(value = "/{pr}/{em}", method = RequestMethod.GET, produces = "application/json")
    public void finalizareComanda(@PathVariable("pr") ArrayList<Integer> produse, @PathVariable("em") String email) {
        comandaService.finalizareComanda(produse,email);
    }
}
