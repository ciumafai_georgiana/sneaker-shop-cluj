import * as React from "react";
import { InputText } from 'primereact/components/inputtext/InputText';
import { Button } from 'primereact/components/button/Button';
import { Message } from 'primereact/components/message/Message';

import { UserService } from "../Services/UserService";
import { NotificationManager } from 'react-notifications';
import '../css/Register.css';
//import signup from '../img/avatar3.png';
import { InputTextarea } from "primereact/inputtextarea";

export class Register extends React.Component {

    constructor(usersProps) {
        super(usersProps);

        this.state = {
            userModel: {
                nume: "",
                telefon: "",
                email: "",
                password: "",
                adresa:""
            },
            errors: {
              nume: "",
                telefon: "",
                email: "",
                password: "",
                adresa:""
               
            },
            isValid: false
        };
    }

    submit() {
        let isValid = UserService.validateUser(this.state.userModel, this.state.errors);

        if (isValid) {
            UserService.addUser(this.state.userModel).then(() => {
                NotificationManager.success("Contul a fost creat !", "Felicitari !");
                this.setState({ isValid: true });
            }, (error) => {
                NotificationManager.error(error.message);
                this.setState({ isValid: false });
            });
        }
        else {
            this.setState({ isValid: false });
        }
    }

    render() {
        return (
            <div id="mainregister">
            
            <div id="addUser">
                <h3>Te rugam sa introduci urmatoarele informatii pentru crearea contului tau!</h3>           
                
                <label> Nume: &nbsp; </label> <br/>
                <InputText id="name" type="text" size={30} onChange={(event) => {
                    let userModel = this.state.userModel;
                    userModel.nume = event.target.value;
                    this.setState({ userModel: userModel });
                }} />
                {this.state.errors.nume !== "" ? <Message severity="warn" text={this.state.errors.nume} /> : null}
                <br /> <br />

                <label> E-mail: &nbsp; </label> <br/>
                <InputText id="mail" type="email" size={30} onChange={(event) => {
                    let userModel = this.state.userModel;
                    userModel.email = event.target.value;
                    this.setState({ userModel: userModel });
                }} />
                {this.state.errors.email !== "" ? <Message severity="warn" text={this.state.errors.email} /> : null}
                <br /><br />

                <label> Parola: &nbsp; </label> <br/>
                <InputText id="password" type="password" size={30} onChange={(event) => {
                    let userModel = this.state.userModel;
                    userModel.password = event.target.value;
                    this.setState({ userModel: userModel });
                }} />
                {this.state.errors.password !== "" ? <Message severity="warn" text={this.state.errors.password} /> : null}
                <br /> <br />

                <label> Telefon:&nbsp;  </label> <br/>
                <InputText id="phone" type="text" size={30} onChange={(event) => {
                    let userModel = this.state.userModel;
                    userModel.telefon = event.target.value;
                    this.setState({ userModel: userModel });
                }} />
                {this.state.errors.telefon !== "" ? <Message severity="warn" text={this.state.errors.telefon} /> : null}
                <br /><br />

                <label> Adresa:&nbsp;  </label> <br/>
                <InputTextarea id="address" type="text" size={200} autoResize="false" onChange={(event) => {
                    let userModel = this.state.userModel;
                    userModel.adresa = event.target.value;
                    this.setState({ userModel: userModel });
                }} />
                
                <br /><br />

                <Button label="Register" onClick={() => this.submit()} />

                {this.state.isValid === true ? this.props.history.push({
                    pathname: '/login',
                    user: this.state.userModel
               }) : null}

            </div>
            </div>
        );
    }
}