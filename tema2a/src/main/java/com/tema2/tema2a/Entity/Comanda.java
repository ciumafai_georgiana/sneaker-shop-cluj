package com.tema2.tema2a.Entity;



import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="comanda")
public class Comanda {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idC;
    @ManyToOne
    @JoinColumn(name="idUser",referencedColumnName = "idUser")
    private User idUser;


    public Comanda(){

    }

    public Comanda(User idUser) {
        this.idUser = idUser;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }
}
