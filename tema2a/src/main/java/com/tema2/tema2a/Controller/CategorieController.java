package com.tema2.tema2a.Controller;

import com.tema2.tema2a.Entity.Categorie;
import com.tema2.tema2a.Service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/Categorie")
public class CategorieController {
    @Autowired
    CategorieService categorieService;



    @RequestMapping(value = "/getAllCategories", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Categorie>> getFoodCategory(){
        List<Categorie> all = categorieService.getAll();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(all);
    }
}
