package com.tema2.tema2a.Repository;

import com.tema2.tema2a.Entity.Produs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface ProdusRepository extends JpaRepository<Produs, Integer> {

    @Query(value = "delete from produs where idp = ?1", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteProdusById(int idP);

    @Query(value = "select * from produs where idp = ?1 and id_categorie = ?2", nativeQuery = true)
    public List<Produs> findProdusByIdPBeforeAndIdCategorie(int idP, int idCategorie);

    @Query(value = "select * from produs where id_categorie = ?1", nativeQuery = true)
    public List<Produs> findProdusByIdCategorie(int idCategorie);

    @Query(value= "select * from produs where id_categorie=?=1 order by price",nativeQuery = true)
    public List<Produs> findProdusByIdCategorieOrderByPrice(int idCategorie);

    @Query(value= "select * from produs where id_categorie=?=1 order by price DESC",nativeQuery = true)
    public List<Produs> findProdusByIdCategorieOrderByPriceDesc(int idCategorie);

    @Query(value= "select * from produs where id_categorie=?=1 order by nume ",nativeQuery = true)
    public List<Produs> findProdusByIdCategorieOrderByNume(int idCategorie);
}
