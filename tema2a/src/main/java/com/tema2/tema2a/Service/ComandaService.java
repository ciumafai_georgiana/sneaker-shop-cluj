package com.tema2.tema2a.Service;

import com.tema2.tema2a.Entity.Comanda;
import com.tema2.tema2a.Entity.ComandaProdus;
import com.tema2.tema2a.Entity.Produs;
import com.tema2.tema2a.Entity.User;
import com.tema2.tema2a.Repository.ComandaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class ComandaService implements IService<Comanda,Integer> {

    @Autowired
    ComandaRepository comandaRepository;

    @Autowired
    UserService userService;

    @Autowired
    ComandaProdusService comandaProdusService;

    @Override
    public boolean add(Comanda element) {
        try{
                comandaRepository.save(element);
                return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public List<Comanda> getAll() {
        return comandaRepository.findAll();
    }

    @Override
    public boolean getById(Integer id) {
        return comandaRepository.existsById(id);
    }

    public void deletecomandaByIdUser(int id){
        comandaRepository.deleteComandaByIdUser(id);
    }
    public void deletecomandabyid(int id){
        comandaRepository.deleteComandaByIdC(id);

    }

    public void finalizareComanda(ArrayList<Integer> produse,String email){
        int iduser=userService.getUserByIdUser(email);
        if(iduser!=0){
            User u= new User();
            u.setIdUser(iduser);
            Comanda c=new Comanda(u);
            add(c);
            for(Integer p: produse){
                Produs prod= new Produs();
                prod.setIdP(p);
                ComandaProdus cp= new ComandaProdus(c,prod);
                comandaProdusService.add(cp);
            }
        }
    }
}
