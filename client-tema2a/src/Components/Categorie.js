import * as React from 'react';
import { Button } from 'primereact/components/button/Button';
//import { RestaurantService } from '../Services/RestaurantService';
import axios from 'axios';

export class Categorie extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        Categories: []
      }
    }
    loadCategories(){
       
        axios.get("http://localhost:8080/Categorie/getAllCategories",this.state)
        .then( response => { 
            
           this.setState({
                Categories: response.data
           })
            
        }).catch( () =>
            console.log("Eroare")
        )
    }
    componentDidMount() {
        this.loadCategories();
    }
    render() {
        return (
            <div>
                {
                    this.state.Categories.map(cat=> (
                        <div key = {cat.idCategorie}>
                        <Button className='btnn' label={cat.numeCategorie} style={{ 'marginTop': `5px`, 'width': '47%' }} 
                            onClick = {()=>{
                            this.props.history.push("/getAllProducts");
                            sessionStorage.setItem("idCategorie",cat.idCategorie);
                        }} 
                        />
                      </div>
                    ))
                }
            </div>
        )                     
    }
}
