package com.tema2.tema2a.Repository;

import com.tema2.tema2a.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("select count(u) from User u where u.email = ?1 and u.password = ?2")
    int getUserByEmailAndPassword(String email, String password);

    @Query("select idUser from User u where u.email=?1 ")
    int getUserByIdUser(String email);

    @Query(value = "update user u set u.password=?1, u.telefon=?2, u.name=?3, u.adresa=?4 where u.email=?5", nativeQuery = true)
    void updateUser(String password, String telefon, String name, String adresa, String email);

   /* @Query(value = "select * from user where username in (select user_job from user_jobs j where j.job_category=?1 and owner = 0)", nativeQuery = true)
    List<User> getJobProviders(int id);*/
}