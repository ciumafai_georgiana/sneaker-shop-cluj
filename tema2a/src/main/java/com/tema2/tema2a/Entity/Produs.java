package com.tema2.tema2a.Entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="produs")
public class Produs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idP;

    @Column(nullable = false)
    private String nume;

    @Column(nullable = false)
    private String descriere;

    @Column(nullable=false)
    private float price;


    @ManyToOne
    @JoinColumn(name="idCategorie",referencedColumnName = "idCategorie")
    private Categorie idCategorie;


    public Produs(){

    }
    public Produs(String nume, String descriere, Categorie idCategorie,Float price) {
        this.nume = nume;
        this.descriere = descriere;
        this.idCategorie = idCategorie;
        this.price=price;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public Categorie getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Categorie idCategorie) {
        this.idCategorie = idCategorie;
    }
}
