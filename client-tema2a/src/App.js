import * as React from 'react';
import './App.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'font-awesome/css/font-awesome.css';
import { BrowserRouter } from 'react-router-dom';
// import { Header } from './Components/Header';
// import { Footer } from './Components/Footer';
import { Router } from './Components/Router';


class App extends React.Component {

  render() {
    return (
      <div className="app">
        <div>
          <BrowserRouter>
            <div className='App site'>
              <div className='site-content'>
                {/* <Header/> */}
                <div className='main'>
                  <Router />
                </div>
              </div>
              {/* <Footer/> */}
            </div>
          </BrowserRouter>
        </div>
      </div>
    );
  }

}

export default App;
