package com.tema2.tema2a.Service;

import com.tema2.tema2a.Entity.Produs;
import com.tema2.tema2a.Repository.ProdusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProdusService implements IService<Produs,Integer> {


    @Autowired
    ProdusRepository produsRepository;
    @Override
    public boolean add(Produs element) {
        try {
            produsRepository.save(element);
            return true;
        }catch (Exception e){
        return false;}

    }

    @Override
    public List<Produs> getAll() {
        return produsRepository.findAll();
    }

    @Override
    public boolean getById(Integer id) {
        return produsRepository.existsById(id);
    }

    public void deletebyid(int id){
        produsRepository.deleteProdusById(id);
    }

    public List<Produs> findProdusByIdCategorie(int idCategory){
        try{
            return produsRepository.findProdusByIdCategorie(idCategory);
        } catch (Exception e) {
            return null;
        }
    }
    public List<Produs> findProdusByIdCategorieOrderByPrice(int idCategory){
        try{
            return produsRepository.findProdusByIdCategorieOrderByPrice(idCategory);
        } catch (Exception e) {
            return null;
        }
    }
    public List<Produs> findProdusByIdCategorieOrderByPriceDesc(int idCategory){
        try{
            return produsRepository.findProdusByIdCategorieOrderByPriceDesc(idCategory);
        } catch (Exception e) {
            return null;
        }
    }
    public List<Produs> findProdusByIdCategorieOrderByNume(int idCategory){
        try{
            return produsRepository.findProdusByIdCategorieOrderByNume(idCategory);
        } catch (Exception e) {
            return null;
        }
    }
}
