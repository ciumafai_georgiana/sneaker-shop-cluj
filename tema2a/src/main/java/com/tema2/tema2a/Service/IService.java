package com.tema2.tema2a.Service;

import java.util.List;

public interface IService<T,ID> {

    boolean add(T element);

    List<T> getAll();

    boolean getById(ID id);

}

