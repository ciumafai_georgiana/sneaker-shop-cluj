package com.tema2.tema2a.Repository;

import com.tema2.tema2a.Entity.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CategorieRepository extends JpaRepository<Categorie,Integer> {

    @Query(value = "delete from categorie where id_categorie = ?1", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteCategorieById(int idCategory);
}
