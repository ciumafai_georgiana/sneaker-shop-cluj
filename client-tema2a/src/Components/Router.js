import * as React from "react";
import { Route, Switch } from 'react-router-dom';
import { Login } from './Login';
import { Dashboard } from './Dashboard';
import { Register } from "./Register";
import {Product} from "./Product";
import { Categorie } from "./Categorie";
import {Detail, Order } from "./Order";
import {Basket} from "./Basket";

export class Router extends React.Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/" exact={true} component={Login}> </Route>
          <Route path="/login" exact={true} component={Login}> </Route>
          <Route path="/dashboard" exact={true} component={Dashboard}> </Route>
          <Route path="/register" exact={true} component={Register}> </Route>
          <Route path="/getAllCategories" exact={true} component={Categorie}> </Route>
          <Route path="/getAllProducts" exact={true} component={Product}> </Route>
          <Route path="/basket" exact={true} component={Basket}></Route>
          <Route path="/order" exact={true} component={Order}></Route>
        </Switch>
      </div>
    );
  }
}

