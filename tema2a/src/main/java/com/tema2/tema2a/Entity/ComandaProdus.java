package com.tema2.tema2a.Entity;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.beans.PropertyDescriptor;

@Entity
@Table(name="comandaprodus")
public class ComandaProdus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCP;

    @ManyToOne
    @JoinColumn(name="idC",referencedColumnName = "idC")
    private Comanda idC;

    @ManyToOne
    @JoinColumn(name = "idP",referencedColumnName = "idP")
    private Produs idP;


    public ComandaProdus(){

    }
    public ComandaProdus(Comanda idC, Produs idP) {
        this.idC = idC;
        this.idP = idP;
    }

    public int getIdCP() {
        return idCP;
    }

    public void setIdCP(int idCP) {
        this.idCP = idCP;
    }

    public Comanda getIdC() {
        return idC;
    }

    public void setIdC(Comanda idC) {
        this.idC = idC;
    }

    public Produs getIdP() {
        return idP;
    }

    public void setIdP(Produs idP) {
        this.idP = idP;
    }
}
