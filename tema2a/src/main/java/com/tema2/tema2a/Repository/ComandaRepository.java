package com.tema2.tema2a.Repository;

import com.tema2.tema2a.Entity.Comanda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ComandaRepository extends JpaRepository<Comanda,Integer> {


    @Query (value = "select * from comanda where id_user=?1",nativeQuery = true)
    void findComandaByIdUser(int idUser);

    @Query (value = "select * from comanda where idc=?1",nativeQuery = true)
    void findComandaByIdC(int idC);


    @Query (value = "delete from comanda where idc=?1",nativeQuery = true)
    @Modifying
    @Transactional
    void deleteComandaByIdC(int idC);

    @Query (value = "delete from comanda where id_user=?1",nativeQuery = true)
    @Modifying
    @Transactional
    void deleteComandaByIdUser(int idUser);


}
