import * as React from 'react';
import { Panel } from 'primereact/panel';
import { TabView, TabPanel } from 'primereact/tabview';
import axios from 'axios';
export class Order extends React.Component {

    constructor() {
        super();
        this.state = {
            products:[],
            email: null
    }
}

    loadOrder(){
        const ide=sessionStorage.getItem("email");
        this.setState({email:ide});

        var retrievedData = localStorage.getItem("bp");
        var pr = JSON.parse(retrievedData);
        this.setState({products:pr});

    
        console.log(this.state.products);
       
    }
    componentDidMount() {
        const ide=sessionStorage.getItem("email");

        var retrievedData = localStorage.getItem("bp");
        var pr = JSON.parse(retrievedData);
    
        var id=[];
        pr.forEach(function(obj){
            id.push(obj.idP);
            });
    
        console.log(id);
        console.log(pr);
        axios.get("http://localhost:8080/Comanda/"+id+"/"+ide)
        .then( response => { 
            console.log("Succes Comanda");
            
        }).catch( () =>
            console.log("Eroare")
        )
    }

    renderProduct(product) {
        return (
            <div style={{ padding: '.5em' }} className="p-col-12 p-md-3">
                <Panel header={product.nume} style={{ textAlign: 'center' }}>
                    <b>Categorie : {product.idCategorie}</b><br></br>
                    <div className="product-descriere">
                        {product.descriere}
                    </div>
                </Panel>
            </div>
        );
    }

    render() {
        return (<React.Fragment>
            {
                <div>
                    <div className="content-section implementation">
                            comanda a avut succes
                    </div>
                </div>
            }
        </React.Fragment>);
    }
}