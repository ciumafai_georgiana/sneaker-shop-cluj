import * as React from 'react';
import axios from 'axios';
import { Button } from 'primereact/components/button/Button';
import {DataView, DataViewLayoutOptions} from 'primereact/dataview';
import { Panel } from 'primereact/panel';
import {Dropdown} from 'primereact/dropdown';

export class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          produse: [],
          layout: 'list',
          selectedPr: null,
          basketId:[],
          basketNume:[],
          basketPrice:[],
          basketProducts:[]
        }
        this.itemTemplate = this.itemTemplate.bind(this);
        
      }
    
      loadAllProducts(){
        //const idR=sessionStorage.getItem("idRestaurant");
        const idC=sessionStorage.getItem("idCategorie");
        
        //const {idR}=this.props.match.params;
       // console.log(idC);
       // console.log("2");
        axios.get("http://localhost:8080/Produs/getAllProductsByIdCategory/"+
                 idC)
        .then( x => { 
            console.log(x.data);
           this.setState({
            
                produse: x.data
           })
            
        }).catch( () =>
            console.log("Eroare")
        )
    }
    itemTemplate(product, layout) {
        if (layout === 'list') {
            return (
                <div className="p-grid">
                    <div>{product.nume}</div>
                    <div>{product.price}</div>
                    <div>{product.descriere}</div>
                    <Button className="add to basket"label={"Add to basket"} style={{ 'marginTop': `5px`, 'width': '47%' }}
                     onClick = {()=>{
                        //this.props.history.push("/basket/"+product.nume);
                       /* sessionStorage.setItem("idP",product.idP);
                        sessionStorage.setItem("nume",product.nume);
                        sessionStorage.setItem("pret",product.price);
                        */

                        this.setState(state=>{state.basketProducts.push(product)});
                        this.setState(state=>{const basketId=state.basketId.push(product.idP)});
                        this.setState(state=>{const basketNume=state.basketNume.push(product.nume)});
                        this.setState(state=>{state.basketPrice.push(product.price)});
                        console.log(this.state.basketProducts);
                        //console.log(this.state.basketId);
                        //console.log(this.state.basketNume);
                        
                    }}  ></Button>
                </div>
            );
        }
        if (layout === 'grid') {
            return (
                <div className="p-col-12 p-md-3">
                    <div>{product.nume}</div>
                    <div>{product.price}</div>
                    
                </div>
            );
        }
    }
    
    
    componentDidMount() {
        console.log("1");
        this.loadAllProducts();
        console.log(this.state);
    }  
    loadAllProducts2(){
        const idC=sessionStorage.getItem("idCategorie");
            axios.get("http://localhost:8080/Produs/getAllProductsByIdCategoryOrderByPrice/"+
                 idC)
             .then( x => { 
                console.log(x.data);
            this.setState({
            
                    produse: x.data
            })
            
             }).catch( () =>
            console.log("Eroare")
            )
    }
    loadAllProducts3(){
        const idC=sessionStorage.getItem("idCategorie");
            axios.get("http://localhost:8080/Produs/getAllProductsByIdCategoryOrderByPriceDesc/"+
                 idC)
             .then( x => { 
                console.log(x.data);
            this.setState({
                    produse: x.data
            })
            
             }).catch( () =>
            console.log("Eroare")
            )
    }
    loadAllProducts4(){
        const idc=sessionStorage.getItem("idCategorie");
        axios.get("http://localhost:8080/Produs/getAllProductsByIdCategoryOrderByNume"+idc).then 
                        (x=>{this.setState({produse:x.data})}).catch(()=>console.log("Error"))
    }
 
    onSortChange(event) {
        let value = event.value;
    
        if (value.indexOf('!') === 0) {
            this.loadAllProducts3();
        }
        else {
            this.loadAllProducts2();
        }
        if(value.indexOf('-')===0){
            this.loadAllProducts4();
        }
    }
    render() {
        const opt = [
            {label: 'Price ascending', value: '!price'},
            {label: 'Price descending',value:'price'},
            {label: 'Brand', value: '-brand'}
        ];
        const header = (
            <div className="p-grid">
                <div className="p-col-12 p-md-4">
                    <Dropdown options={opt} value={this.state.sortKey} placeholder="Sort By" onChange={(event)=>(event.value.indexOf('!')===0? this.loadAllProducts2() : this.loadAllProducts3()
                                                                                                                | event.value.indexOf("-")===0? this.loadAllProducts4(): this.loadAllProducts4())
                                                                                                                
                                                                                                                } />
                                                                    
                </div>
            </div>
        );
       
        return (
            <div>
                {
                    <div className="p-grid p-fluid">
                       <DataView className='p-user-list' value={this.state.produse} header={header} layout={this.state.layout} itemTemplate={this.itemTemplate}></DataView>  
                       <Button className="p-button-success"label={"See basket"} style={{ 'color':'black','marginTop': `5px`, 'width': '47%' }}
                       
                       onClick = {()=>{
                        this.props.history.push("/basket");
                        
                        sessionStorage.setItem("basketId",this.state.basketId);
                        sessionStorage.setItem("basketNume",this.state.basketNume);
                        sessionStorage.setItem("basketPrice",this.state.basketPrice);
                        sessionStorage.setItem("basketProducts",this.state.basketProducts);
                        localStorage.setItem("bp", JSON.stringify(this.state.basketProducts));
                      
                        
                    }} ></Button>
                    </div>   
                  
                }
            </div>
        )                     
    }
}