package com.tema2.tema2a.Controller;

import com.tema2.tema2a.Entity.Categorie;
import com.tema2.tema2a.Entity.Produs;
import com.tema2.tema2a.Service.CategorieService;
import com.tema2.tema2a.Service.ProdusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/Produs")
public class ProdusController {
    @Autowired
    ProdusService produsService;



    @RequestMapping(value = "/getAllProductsByIdCategory/{idcat}", method = RequestMethod.GET, produces = "application/json")
    public List<Produs> getAllProductsByIdCategory(@PathVariable("idcat") String idcat) {
        return produsService.findProdusByIdCategorie(Integer.parseInt(idcat));
    }
    @RequestMapping(value = "/getAllProductsByIdCategoryOrderByPrice/{idcat}", method = RequestMethod.GET)
    public List<Produs> getAllProductsByIdCategoryOrderByPrice(@PathVariable("idcat") String idcat) {
        return produsService.findProdusByIdCategorieOrderByPrice(Integer.parseInt(idcat));
    }
    @RequestMapping(value = "/getAllProductsByIdCategoryOrderByPriceDesc/{idcat}", method = RequestMethod.GET)
    public List<Produs> getAllProductsByIdCategoryOrderByPriceDesc(@PathVariable("idcat") String idcat) {
        return produsService.findProdusByIdCategorieOrderByPriceDesc(Integer.parseInt(idcat));
    }
    @RequestMapping(value = "/getAllProductsByIdCategoryOrderByNume/{idcat}", method = RequestMethod.GET)
    public List<Produs> getAllProductsByIdCategoryOrderByNume(@PathVariable("idcat") String idcat) {
        return produsService.findProdusByIdCategorieOrderByNume(Integer.parseInt(idcat));
    }
}
