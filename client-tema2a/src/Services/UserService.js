import axios from 'axios';
export class UserService{
    static root = 'http://localhost:8080';

    static addUser(userModel){
        return new Promise((resolve, reject )=> {
            axios.post(this.root + "/Users/register", userModel).then((response) => {
                resolve(response);
            }, (error) => {
                reject(error.response.data);
            })
        })
    }

    static getAllUsernames() {
        return new Promise((resolve, reject) => {
            axios.get(this.root + "/Users/getAllUsers").then((response) => {
                resolve(response.data);
            }, (error) => {
                reject(error);
            });
        })
    }

    static getUser(email){
        return new Promise((resolve, reject)=> {
            axios.get(this.root + "/User/getUser",{
                params:{
                    email : email
                }
            }).then((response) => {
                resolve(response.data);
            }, (error) => {
                reject(error.response.data);
            })
        })
    }

    static updateUser(userModel){
        return new Promise((resolve, reject)=> {
            axios.post(this.root + "/Users/updateAccount", userModel).then((response) => {
                resolve(response.data);
            }, (error) => {
                reject(error.response.data);
            })
        })
    }


    static validateUser(userModel, errors)
    {
        let isValid =  true;
        let expr1 = /^[0-9a-zA-z_-]+$/

        if(userModel.password === "")
        {
            errors.password = "Parola nu poate fi vida";
            isValid = false;
        }
        else
        {
            errors.password = "";
        }

        if(userModel.nume === "")
        {
            errors.nume = "Nume nu poate fi vid";
            isValid = false;
        }
        else
        {
            errors.nume = "";
        }

        expr1 = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(userModel.email === "" || userModel.email.match(expr1) === null)
        {
            errors.email = "E-mail invalid";
            isValid = false;
        }
        else
        {
            errors.email = "";
        }

        expr1 = "[0-9]{10}";
        if(userModel.telefon === "" || userModel.telefon.match(expr1) === null)
        {
            errors.telefon = "Telefon invalid";
            isValid = false;
        }
        else
        {
            errors.telefon = "";
        }

        return isValid;
    }
}