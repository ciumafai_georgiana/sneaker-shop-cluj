package com.tema2.tema2a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.tema2.tema2a.Repository")
public class Tema2aApplication {


		public static void main (String[]args){
			SpringApplication.run(Tema2aApplication.class, args);

		}


}
